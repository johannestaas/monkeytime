import timeit_old
import timeit_new

print('Testing builtin strptime')
old_runs = timeit_old.test(n=500000)
print('Testing monkeytime strptime')
new_runs = timeit_new.test(n=500000)

runs = []
for i in range(len(old_runs)):
    old = old_runs[i]
    new = new_runs[i]
    perf = float(old) / float(new)
    print('%2f times as fast' % perf)
    runs += [perf]

avg = sum(runs) / len(runs)
print('Average: %2f times as fast' % avg)
