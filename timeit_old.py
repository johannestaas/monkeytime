#!/usr/bin/env python
import timeit

def test(n=1000000):
    setup = """
from datetime import datetime
"""

    run = 'dt = datetime.strptime("%s", "%s")\n'

    tests = (
        ('2015-01-02 03:04:05.001234', '%Y-%m-%d %H:%M:%S.%f'),
        ('05-06 12:15:18', '%m-%d %H:%M:%S'),
        ('2010', '%Y'),
        ('1905/08/05', '%Y/%m/%d'),
        ('14:05:03.123456', '%H:%M:%S.%f'),
    )

    runs = []
    for args in tests:
        print('testing %s' % (args,))
        test_run = run % args
        old = timeit.timeit(stmt=test_run, number=n, setup=setup)
        print('%s seconds' % old)
        runs += [old]
    return runs

if __name__ == '__main__':
    test()
