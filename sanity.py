#!/usr/bin/env python

from monkeytime import datetime
from datetime import datetime

def printdt(dt):
    print dt
    print dt.__class__.__name__
    print dt.year
    print dt.month
    print dt.day
    print dt.hour
    print dt.minute
    print dt.second
    print dt.microsecond

dt = datetime.strptime('2010-11-12 13:14:09 012345', '%Y-%m-%d %H:%M:%S %f')
printdt(dt)
dt = datetime.strptime('11-12 13: 14', '%m-%d %H: %M')
printdt(dt)

